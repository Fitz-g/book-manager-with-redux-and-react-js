import { Route, Routes } from "react-router-dom";
import "./App.css";
import Footer from "./components/Footer";
import NavBar from "./components/NavBar";
import AddBooks from "./containers/AddBooks";
import SearchBooks from "./containers/SearchBooks";

function App() {
  return (
    <>
      <NavBar />
      <Routes>
        <Route path="/" element={<AddBooks />} />
        <Route path="/search" element={<SearchBooks />} />
      </Routes>
      <Footer />
    </>
  );
}

export default App;
