import axios from "axios";
import {
  FETCH_BOOKS_ERROR,
  FETCH_BOOKS_LOADING,
  FETCH_BOOKS_SUCCESS,
} from "../constances";

export const fetchBooksLoading = () => {
  return {
    type: FETCH_BOOKS_LOADING,
  };
};

export const fetchBooksSuccess = (data) => {
  return {
    type: FETCH_BOOKS_SUCCESS,
    payload: data,
  };
};

export const fetchBooksError = (error) => {
  return {
    type: FETCH_BOOKS_ERROR,
    payload: error,
  };
};

export const fetchBooks = (search) => {
  return (dispatch) => {
    dispatch(fetchBooksLoading());
    axios
      .get(
        `https://www.googleapis.com/books/v1/volumes?q=${search}&maxResults=20`
      )
      .then((response) => {
        dispatch(fetchBooksSuccess(response.data.items));
      })
      .catch((error) => {
        dispatch(fetchBooksError(error.message));
      });
  };
};
