import { v4 as uuidv4 } from "uuid";
import { ADD_BOOKS, DELETE_BOOK, DELETE_ALL_BOOKS } from "../constances";

const initialState = {
  books: [],
};

const helperAddData = (action) => {
  return {
    id: uuidv4(),
    title: action.title,
    author: action.author,
  };
};

const helperDeleteBookById = (state, id) => {
  const books = state.filter((book) => book.id !== id);
  return books;
};

const addBooksReducer = (state = initialState.books, action) => {
  if (localStorage.getItem("booksData")) {
    state = JSON.parse(localStorage.getItem("booksData"));
  }
  switch (action.type) {
    case ADD_BOOKS:
      state = [...state, helperAddData(action.payload)];
      localStorage.setItem("booksData", JSON.stringify(state));
      return state;
    case DELETE_BOOK:
      state = helperDeleteBookById(state, action.payload);
      localStorage.setItem("booksData", JSON.stringify(state));
      return state;
    case DELETE_ALL_BOOKS:
      state = [];
      localStorage.setItem("booksData", JSON.stringify(state));
      return state;
    default:
      return state;
  }
};

export default addBooksReducer;
