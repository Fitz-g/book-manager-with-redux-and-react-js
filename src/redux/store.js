import { applyMiddleware, combineReducers, createStore } from "redux";
import addBooksReducer from "./reducers/addBooksReducer";
import thunk from "redux-thunk";
import fetchBooksReducer from "./reducers/fetchBooksReducer";

const rootReducer = combineReducers({
  library: addBooksReducer,
  search: fetchBooksReducer,
});

const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;
