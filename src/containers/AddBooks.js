import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  addBooks,
  deleteBook,
  deleteAllBooks,
} from "../redux/actions/addBooks";
import FlipMove from "react-flip-move";

function AddBooks() {
  const libraryData = useSelector((state) => state.library);
  const dispatch = useDispatch();

  const initialState = {
    title: "",
    author: "",
  };

  const [newData, setNewData] = useState(initialState);

  const handleInputChange = (e) => {
    setNewData({
      ...newData,
      [e.target.name]: e.target.value,
    });
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();
    dispatch(addBooks(newData));
    setNewData(initialState);
  };

  return (
    <main role="main">
      <div className="p-5 mb-4 bg-light">
        <div className="container text-center">
          <h1 className="display-4">BOOKS</h1>
          <p>Ajoutez un livre à votre bibliothèque</p>
          <form
            className="form-inline justify-content-center"
            onSubmit={handleFormSubmit}
          >
            <div className="row">
              <div className="col">
                <input
                  className="form-control"
                  type="text"
                  placeholder="Titre"
                  name="title"
                  value={newData.title}
                  onChange={handleInputChange}
                  required
                />
              </div>
              <div className="col">
                <input
                  className="form-control ml-3"
                  type="text"
                  placeholder="Auteur"
                  name="author"
                  value={newData.author}
                  onChange={handleInputChange}
                  required
                />
              </div>
              <div className="col-2">
                <button className="btn btn-outline-secondary">
                  Ajouter un livre
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>

      <div className="container" style={{ minHeight: "200px" }}>
        <div className="row justify-content-evenly">
          <div className="col-md-12">
            <ul className="list-group">
              {libraryData.length > 0 ? (
                <FlipMove>
                  {libraryData.map((data) => (
                    <li
                      key={data.id}
                      className="align-items-center list-group-item list-group-light d-flex justify-content-between"
                    >
                      <span>
                        <strong>Titre : </strong> {data.title}
                      </span>
                      <span>
                        <strong>Auteur : </strong> {data.author}
                      </span>
                      <span>
                        <button
                          className="btn btn-danger"
                          onClick={() => dispatch(deleteBook(data.id))}
                        >
                          X
                        </button>
                      </span>
                    </li>
                  ))}
                </FlipMove>
              ) : (
                <p className="text-center"> Aucune données</p>
              )}
            </ul>
            <div className="d-flex justify-content-center">
              {libraryData.length > 0 && (
                <button
                  className="btn btn-danger my-5"
                  onClick={() => dispatch(deleteAllBooks())}
                >
                  Effacer tous les livres
                </button>
              )}
            </div>
          </div>
        </div>
      </div>
    </main>
  );
}

// const mapStateToProps = (state) => {
//   return {
//     libraryData: state.library,
//   };
// };
//
// const mapDispatchToProps = (dispatch) => {
//   return {
//     addBooks: (param) => dispatch(addBooks(param)),
//   };
// };

// export default connect(mapStateToProps, mapDispatchToProps)(AddBooks);

export default AddBooks;
