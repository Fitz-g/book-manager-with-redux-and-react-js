import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchBooks } from "../redux/actions/fetchBooks";
import { addBooks } from "../redux/actions/addBooks";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function SearchBooks() {
  const [bookToSearch, setBookToSearch] = useState("");
  const state = useSelector((state) => state.search);
  const dispatch = useDispatch();

  const handleSaveInfo = (title, author) => {
    dispatch(addBooks({ title, author }));

    toast("🦄 Livre enregistrer", {
      position: "bottom-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "dark",
    });
  };

  const handleSubmitSearch = (e) => {
    e.preventDefault();
    dispatch(fetchBooks(bookToSearch));
  };

  const displayFetchedBooks = state.isLoading ? (
    <div className="d-flex justify-content-center">
      <div class="spinner-border text-info" role="status">
        <span class="visually-hidden">Loading...</span>
      </div>
    </div>
  ) : state.error ? (
    <p className="text-danger">{state.error}</p>
  ) : (
    state.fetchedBooks.map((item) => {
      return (
        <div className="accordion-item" key={item.id}>
          <h5 className="accordion-header" id={item.id}>
            <button
              className="accordion-button"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target={`#${item.id}`}
              aria-expanded="false"
              aria-controls={`${item.id}`}
            >
              {item.volumeInfo.title}
            </button>
          </h5>
          <div
            id={item.id}
            className="accordion-collapse collapse show"
            aria-labelledby={item.id}
            data-bs-parent="#accordion"
          >
            <div className="card-body p-4">
              {item.volumeInfo.hasOwnProperty("imageLinks") && (
                <img
                  src={item.volumeInfo.imageLinks.thumbnail ?? ""}
                  alt={item.volumeInfo.title}
                />
              )}

              <br />
              <h4 className="card-title">Titre : {item.volumeInfo.title}</h4>
              <h5 className="card-title">Auteur : {item.volumeInfo.authors}</h5>
              <p className="card-text">{item.volumeInfo.description}</p>
              <a
                className="btn btn-outline-secondary"
                target="_blank"
                rel="noopener noreferrer"
                href={item.volumeInfo.previewLink}
              >
                Plus d'infos
              </a>
              <button
                className="btn btn-outline-secondary"
                onClick={(e) =>
                  handleSaveInfo(item.volumeInfo.title, item.volumeInfo.authors)
                }
              >
                Enregistrer
              </button>
              <ToastContainer
                position="bottom-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="dark"
              />
            </div>
          </div>
        </div>
      );
    })
  );

  return (
    <main role="main">
      <div className="p-5 mb-4 bg-light">
        <div className="container text-center">
          <h1 className="display-4">BOOKS</h1>
          <p>Indiquez le sujet du livre à rechercher</p>
          <form
            className="form-inline justify-content-center"
            onSubmit={handleSubmitSearch}
          >
            <div className="row">
              <div className="col">
                <input
                  className="form-control"
                  type="text"
                  placeholder="Rechercher..."
                  required
                  value={bookToSearch}
                  onChange={(e) => setBookToSearch(e.target.value)}
                />
              </div>
              <div className="col-2">
                <button className="btn btn-outline-secondary ml-3">
                  Rechercher
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div className="container" style={{ minHeight: "200px" }}>
        <div class="accordion" id="accordion">
          {displayFetchedBooks}
        </div>
      </div>
    </main>
  );
}

export default SearchBooks;
